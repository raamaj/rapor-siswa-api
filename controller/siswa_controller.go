package controller

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/raamaj/rapor-siswa-api/services"
)

func CreateSiswa(ctx *gin.Context) {
	services.CreateSiswa(ctx)
}

func GetAllSiswa(ctx *gin.Context) {
	services.ReadSiswas(ctx)
}

func GetSiswaById(ctx *gin.Context) {
	services.GetSiswaById(ctx)
}

func UpdateSiswa(ctx *gin.Context) {
	services.UpdateSiswa(ctx)
}

func DeleteSiswa(ctx *gin.Context) {
	services.DeleteSiswa(ctx)
}
