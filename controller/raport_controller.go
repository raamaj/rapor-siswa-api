package controller

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/raamaj/rapor-siswa-api/services"
)

func GetAllRaport(ctx *gin.Context) {
	services.ReadRaports(ctx)
}

func GetRaportById(ctx *gin.Context) {
	services.GetRaportById(ctx)
}

func CreateRaport(ctx *gin.Context) {
	services.CreateRaport(ctx)
}

func UpdateRaport(ctx *gin.Context) {
	services.UpdateRaport(ctx)
}

func DeleteRaport(ctx *gin.Context) {
	services.DeleteRaport(ctx)
}
