package services

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/raamaj/rapor-siswa-api/models"
	"gitlab.com/raamaj/rapor-siswa-api/models/web"
	"gitlab.com/raamaj/rapor-siswa-api/repository"
	"gitlab.com/raamaj/rapor-siswa-api/utils"
)

func ReadRaports(ctx *gin.Context) {
	var raports []web.RaportSiswa

	err := repository.GetAllRaport(&raports)

	if err != nil {
		fmt.Println(err.Error())
		ctx.JSON(http.StatusNotFound, "Data not found")
	} else {
		ctx.JSON(http.StatusOK, raports)
	}
}

func GetRaportById(ctx *gin.Context) {
	var raport web.RaportSiswa
	id := ctx.Params.ByName("id")
	err := repository.GetRaportById(&raport, id)
	if err != nil {
		fmt.Println(err.Error())
		ctx.JSON(http.StatusNotFound, "Data not found")
	} else {
		ctx.JSON(http.StatusOK, raport)
	}
}

func CreateRaport(ctx *gin.Context) {
	var raport models.Raport
	ctx.BindJSON(&raport)

	validate, trans := utils.ValidatorTemplate()
	valid := validate.Struct(raport)
	errs := utils.TranslateError(valid, trans)

	if errs != nil {
		fmt.Println(errs)
		ctx.JSON(http.StatusBadRequest, gin.H{"message": errs})
	} else {
		err := repository.CreateRaport(&raport)
		if err != nil {
			fmt.Println(err.Error())
			ctx.JSON(http.StatusBadRequest, "Failed to insert Report")
		} else {
			ctx.JSON(http.StatusOK, "Report inserted successfully")
		}
	}
}

func UpdateRaport(ctx *gin.Context) {
	var raport models.Raport
	ctx.BindJSON(&raport)

	validate, trans := utils.ValidatorTemplate()
	valid := validate.Struct(raport)
	errs := utils.TranslateError(valid, trans)

	if errs != nil {
		fmt.Println(errs)
		ctx.JSON(http.StatusBadRequest, gin.H{"message": errs})
	} else {
		err := repository.UpdateRaport(&raport)
		if err != nil {
			fmt.Println(err.Error())
			ctx.JSON(http.StatusBadRequest, "Failed to update Report")
		} else {
			ctx.JSON(http.StatusOK, "Report updated successfully")
		}
	}
}

func DeleteRaport(ctx *gin.Context) {
	var raport models.Raport
	ctx.BindJSON(&raport)

	err := repository.DeleteRaport(&raport)
	if err != nil {
		fmt.Println(err.Error())
		ctx.JSON(http.StatusBadRequest, "Failed to delete Report!")
	} else {
		ctx.JSON(http.StatusOK, "Report deleted successfully!")
	}
}
