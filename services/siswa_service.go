package services

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/raamaj/rapor-siswa-api/models"
	"gitlab.com/raamaj/rapor-siswa-api/repository"
	"gitlab.com/raamaj/rapor-siswa-api/utils"
)

func CreateSiswa(ctx *gin.Context) {
	var siswa models.Siswa
	ctx.BindJSON(&siswa)

	validate, trans := utils.ValidatorTemplate()

	valid := validate.Struct(siswa)
	errs := utils.TranslateError(valid, trans)

	if errs != nil {
		fmt.Println(errs)
		ctx.JSON(http.StatusBadRequest, gin.H{"message": errs})
	} else {
		err := repository.SiswaSave(&siswa)

		if err != nil {
			fmt.Println(err.Error())
			ctx.JSON(http.StatusBadRequest, "Student already exists")
		} else {
			ctx.JSON(http.StatusOK, siswa)
		}
	}
}

func ReadSiswas(ctx *gin.Context) {
	var siswas []models.Siswa
	err := repository.SiswaGetAll(&siswas)
	if err != nil {
		fmt.Println(err.Error())
		ctx.JSON(http.StatusNotFound, "Data not found")
	} else {
		ctx.JSON(http.StatusOK, siswas)
	}
}

func GetSiswaById(ctx *gin.Context) {
	var siswa models.Siswa
	err := repository.SiswaGetById(&siswa, ctx.Params.ByName("id"))
	if err != nil {
		fmt.Println(err.Error())
		ctx.JSON(http.StatusNotFound, "Data not found")
	} else {
		ctx.JSON(http.StatusOK, siswa)
	}
}

func UpdateSiswa(ctx *gin.Context) {
	var siswa models.Siswa
	ctx.BindJSON(&siswa)

	validate, trans := utils.ValidatorTemplate()

	valid := validate.Struct(siswa)
	errs := utils.TranslateError(valid, trans)

	if errs != nil {
		fmt.Println(errs)
		ctx.JSON(http.StatusBadRequest, gin.H{"message": errs})
	} else {
		err := repository.SiswaUpdate(&siswa)
		if err != nil {
			fmt.Println(err.Error())
			ctx.JSON(http.StatusBadRequest, "Failed to update Student")
		} else {
			ctx.JSON(http.StatusOK, "Student updated successfully")
		}
	}
}

func DeleteSiswa(ctx *gin.Context) {
	var siswa models.Siswa
	ctx.BindJSON(&siswa)

	err := repository.SiswaDelete(&siswa)
	if err != nil {
		fmt.Println(err.Error())
		ctx.JSON(http.StatusBadRequest, "Failed to delete Student")
	} else {
		ctx.JSON(http.StatusOK, "Student deleted successfully")
	}
}
