package repository

import (
	"gitlab.com/raamaj/rapor-siswa-api/config"
	"gitlab.com/raamaj/rapor-siswa-api/models"
	"gitlab.com/raamaj/rapor-siswa-api/models/web"
)

func GetAllRaport(raports *[]web.RaportSiswa) (err error) {
	if err = config.DB.Raw("SELECT r.id, s.name, r.mat, r.indo, r.eng, r.ipa, r.ips FROM raports r LEFT JOIN siswas s ON r.id_siswa = s.id").Scan(&raports).Error; err != nil {
		return err
	}

	return nil
}

func GetRaportById(raport *web.RaportSiswa, id string) (err error) {
	if err = config.DB.Raw("SELECT r.id, s.name, r.mat, r.indo, r.eng, r.ipa, r.ips FROM raports r LEFT JOIN siswas s ON r.id_siswa = s.id WHERE r.id_siswa = ?", id).Scan(&raport).Error; err != nil {
		return err
	}

	return nil
}

func CreateRaport(raport *models.Raport) (err error) {
	if err = config.DB.Create(raport).Error; err != nil {
		return err
	}

	return nil
}

func UpdateRaport(raport *models.Raport) (err error) {
	if err = config.DB.Save(raport).Error; err != nil {
		return err
	}

	return nil
}

func DeleteRaport(raport *models.Raport) (err error) {
	if err = config.DB.Delete(raport).Error; err != nil {
		return err
	}

	return nil
}
