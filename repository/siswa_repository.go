package repository

import (
	"gitlab.com/raamaj/rapor-siswa-api/config"
	"gitlab.com/raamaj/rapor-siswa-api/models"

	_ "github.com/go-sql-driver/mysql"
)

func SiswaSave(siswa *models.Siswa) (err error) {
	if err = config.DB.Debug().Create(siswa).Error; err != nil {
		return err
	}
	return nil
}

func SiswaGetAll(siswas *[]models.Siswa) (err error) {
	if err = config.DB.Raw("SELECT * FROM siswas").Scan(&siswas).Error; err != nil {
		return err
	}

	return nil
}

func SiswaGetById(siswa *models.Siswa, id string) (err error) {
	if err = config.DB.Where("id = ?", id).First(siswa).Error; err != nil {
		return err
	}

	return nil
}

func SiswaUpdate(siswa *models.Siswa) (err error) {
	if err = config.DB.Save(siswa).Error; err != nil {
		return err
	}

	return nil
}

func SiswaDelete(siswa *models.Siswa) (err error) {
	if err = config.DB.Delete(siswa).Error; err != nil {
		return err
	}

	return nil
}
