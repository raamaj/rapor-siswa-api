package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/raamaj/rapor-siswa-api/controller"
)

func SetupRouter() *gin.Engine {
	routes := gin.Default()
	siswaRoute := routes.Group("/siswa-api")
	{
		siswaRoute.POST("siswa", controller.CreateSiswa)
		siswaRoute.GET("siswa", controller.GetAllSiswa)
		siswaRoute.PUT("siswa", controller.UpdateSiswa)
		siswaRoute.GET("siswa/:id", controller.GetSiswaById)
		siswaRoute.DELETE("siswa", controller.DeleteSiswa)
	}

	raportRoute := routes.Group("/raport-api")
	{
		raportRoute.GET("raport", controller.GetAllRaport)
		raportRoute.GET("raport/:id", controller.GetRaportById)
		raportRoute.POST("raport", controller.CreateRaport)
		raportRoute.PUT("raport", controller.UpdateRaport)
		raportRoute.DELETE("raport", controller.DeleteRaport)
	}

	return routes
}
