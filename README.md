# Rapor Siswa Api



## Description

A simple API Student and Report with Go, Gin, and Gorm

## APIs
| URL | METHOD | Description |
| :----- | :-: | :- |
| /siswa-api/siswa | POST | Create new Student data |
| /siswa-api/siswa | GET | Get Students data |
| /siswa-api/siswa | PUT | Update Student data |
| /siswa-api/siswa/{id} | GET | Get Student data |
| /siswa-api/siswa | DELETE | Delete Student data |
| /raport-api/raport | POST | Create new Report data |
| /raport-api/raport | GET | Get Reports data |
| /raport-api/raport | PUT | Update Report data |
| /raport-api/raport/{id}| GET | Get Report data |
| /raport-api/raport| DELETE | Delete Report data |

