package main

import (
	"fmt"

	"github.com/jinzhu/gorm"
	"gitlab.com/raamaj/rapor-siswa-api/config"
	"gitlab.com/raamaj/rapor-siswa-api/models"
	"gitlab.com/raamaj/rapor-siswa-api/routes"

	_ "github.com/go-sql-driver/mysql"
)

var err error

func main() {
	config.DB, err = gorm.Open("mysql", config.DbURL(config.BuildDBConfig()))

	if err != nil {
		fmt.Println("Status:", err)
	}

	defer config.DB.Close()
	config.DB.AutoMigrate(&models.Siswa{})
	config.DB.AutoMigrate(&models.Raport{}).AddForeignKey("id_siswa", "siswas(id)", "CASCADE", "CASCADE")

	r := routes.SetupRouter()

	r.Run()
}
