package web

type RaportSiswa struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Mat  int    `json:"mat"`
	Indo int    `json:"indo"`
	Eng  int    `json:"eng"`
	Ipa  int    `json:"ipa"`
	Ips  int    `json:"ips"`
}
