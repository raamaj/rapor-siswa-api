package models

type Raport struct {
	ID      int `gorm:"primary_key;auto_increment" json:"id"`
	IdSiswa int `validate:"required" gorm:"not null;unique" json:"id_siswa"`
	Mat     int `validate:"required,numeric" json:"mat"`
	Indo    int `validate:"required,numeric" json:"indo"`
	Eng     int `validate:"required,numeric" json:"eng"`
	Ipa     int `validate:"required,numeric" json:"ipa"`
	Ips     int `validate:"required,numeric" json:"ips"`
}
