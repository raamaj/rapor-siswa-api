package models

type Siswa struct {
	ID      int    `gorm:"primary_key;auto_increment" json:"id"`
	Name    string `validate:"required,min=3,max=35" gorm:"size:255;not null;unique" json:"name"`
	Phone   string `validate:"required,numeric,min=11,max=13" gorm:"size:255;not null;unique" json:"phone"`
	Address string `validate:"required,max=255" gorm:"size:255;not null" json:"address"`
	Email   string `validate:"required,email" gorm:"size:255;not null;unique" json:"email"`
}
